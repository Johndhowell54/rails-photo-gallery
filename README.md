# README

Rails learning project I made a few months ago before beginning a larger Rails project for my employer

# Gems used
Active Storage

# Database
Local mysql database, credentials stored in an env variable

# Screenshots
All uploaded pictures
![gallery](./photo_gallery_index.png)

Upload screen
![upload](./photo_gallery_upload.png)
