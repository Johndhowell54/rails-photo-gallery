class PicturesController < ApplicationController
  def index
    @pics = Picture.all
  end

  def show
    @pic = Picture.find(params[:id])
  end
  def new
    @pic = Picture.new
  end

  def create
    @pic = Picture.create!(image: pic_params)

    if @pic.save
      redirect_to @pic
    else
      render :new, status: :unprocessable_entity
    end
  end

  private
  def pic_params
    params.require(:image)
  end

end
