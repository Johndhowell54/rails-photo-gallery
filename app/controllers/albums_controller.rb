class AlbumsController < ApplicationController
  def index
    @albums = Album.all
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
    @album = Album.new
  end

  def create
    @album = Album.create!(title: alb_params)

    if @album.save
      redirect to @album
    else
      render :new, status: :unprocessable_entity
    end
  end

    private
    def album_params
      params.require(:title)
    end
end
